<?php
/**
 * Footer template: Default
 * 
 * @package education_pack_Starter_Theme
 */

$class_default = '';
$education_pack_options = get_theme_mods();

if ( ! education_pack_plugin_active( 'thim-core' ) ) {
	$class_default = 'no-padding';
}
?>
<div class="footer <?php echo esc_attr($class_default); ?>">
	<div class="container">
		<div class="row">
			<?php education_pack_footer_widgets(); ?>
		</div>
	</div>
</div>

<div class="copyright-area">
	<div class="container">
		<div class="copyright-content">
			<div class="row">
				<div class="col-sm-12 text-center">
          Copyright by ITC
				</div>
			</div>
		</div>
	</div>
</div>